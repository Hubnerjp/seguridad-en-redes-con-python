#Cifrado Cesar

#La cadena a encriptar y desencriptar:
message = "El mensaje secreto es UNSCH."

#La clave para encriptar/desencriptar
key = 13

#Indica al programa que cifre o descifre
mode = "encrypt" # Asignar cifrar o descifrar

#Cada simbolo posible que se puede cifrar
LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

#Almacena la forma cifrada/descifrada del mensaje
translated = ""

#Capitalizar la cadena en el mensaje
message = message.upper()

#Ejecute el codigo de cifrado/descifrado
#en cada simbolo de la cadena de mensajes
for symbol in message:
    if symbol in LETTERS:
        #Obtener el numero cifrado (o descifrado) para este simbolo
        num = LETTERS.find(symbol) #Obtener el numero del simbolo
        if mode == "encrypt":
            num = num + key
        elif mode == "decrypt":
            num = num - key       
        if num >= len(LETTERS):
            num = num - len(LETTERS)
        elif num < 0:
            num = num + len(LETTERS)
        #Agregar el simbolo de cifrado/ descifrado 
        #al final de la variable translated
        translated = translated + LETTERS[num]

    else:
        #Simplemente agregua el simbolo sin cifrar/descifrar
        translated = translated + symbol

#Imprime el texto cifrado, descifrado y la clave
print("Texto Claro :",message)
print("Clave key =",key)
print("Texto Cifrado :",translated)

