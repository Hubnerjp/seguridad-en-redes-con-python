# Transposition Cipher Test
import random, sys, descifradoTransposicion, cifradoTransposicion

def main():
    random.seed(42) # set the random "seed" to a static value

    for i in range(20): # run 20 tests
        # Generate random messages to test.

        # The message will have a random length:
        message = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" * random.randint(2, 3)
        
        # Convert the message string to a list to shuffle it.
        message = list(message)
        random.shuffle(message)
        message = "".join(message) # convert list to string

        print("Mensaje aleatorio #% s: % s... " % (i+1, message[:25]))

        # Check all possible keys for each message.
        for key in range(1, len(message)):
            encrypted = cifradoTransposicion.encryptMessage(key, message)
            decrypted = descifradoTransposicion.decryptMessage(key, encrypted)

            # If the decryption doesn t match the original message, display
            # an error message and quit.
            if message != decrypted:
                print("Mismatch with key % s and message % s." % (key, message))
                print(decrypted)
                sys.exit()

    print("La prueba de cifrado de transposicion se ejecuto...")


# If transpositionTest.py is run (instead of imported as a module) call
# the main() function.
if __name__ == "__main__":
    main()
